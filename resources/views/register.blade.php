<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="POST">
		@csrf
		<label for="fname">First name:</label><br><br>
		<input type="text" id="fname" name="fname"><br><br>
		<label for="lname">Last name:</label><br><br>
		<input type="text" id="lname" name="lname"><br><br>
		<label for="gender">Gender:</label><br><br>
		<input type="radio" id="male" name="gender" value="male">Male<br>
		<input type="radio" id="female" name="gender" value="female">Female<br>
		<input type="radio" id="other" name="gender" value="other">Other<br><br>
		<label for="nationality">Nationality:</label><br><br>
		<select name="nationality" id="nationality">
			<option value="Indonesian">Indonesian</option>
			<option value="Singaporian">Singaporian</option>
			<option value="Malasian">Malasian</option>
			<option value="Australian">Australian</option>
		</select><br><br>
		<label for="language">Language Spoken:</label><br><br>
		<input type="checkbox" id="indo" name="indo" value="Indonesia">Bahasa Indonesia<br>
		<input type="checkbox" id="english" name="english" value="English">English<br>
		<input type="checkbox" id="other" name="other" value="Other">Other<br><br>
		<label for="bio">Bio:</label><br><br>
		<textarea id="bio" name="bio" rows="10" cols="30"></textarea><br><br>
		


		<input type="submit" value="Sign Up">
	</form> 
</body>
</html>